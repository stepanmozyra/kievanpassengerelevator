<?php
namespace ElevatorVendor\PassengerElevator\Doors;

use ElevatorVendor\PassengerElevator\ElevatorControls\ElevatorControl as ElevatorControl;
use ElevatorVendor\PassengerElevator\Dispatchers\Dispatcher as Dispatcher;

/**
 * Class DoorControl
 * @author Artyom Gordiyevsky <kievan@gmail.com>
 */
class DoorControl implements ElevatorControl {
    /**
     * @var int $doorState Door state.
     */
    private $doorState;
    /**
     * @var Door $door Door object.
     */
    private $door;

    /**
     * DoorControl constructor.
     */
    function __construct(){
    }

    /**
     * Updates dispatcher about a door state.
     * @return void
     */
    function update(){
        Dispatcher::getInstance()->setDoorState($this->doorState);
    }

    /**
     * Opens a door, waits for a certain period of time, then closes the door.
     * @param Door $door Door object.
     * @return void
     */
    function open(Door $door){
        $this->door = $door;
        $door->open();
        $this->doorState = Dispatcher::DOOR_STATE_OPENED;
        Dispatcher::getInstance()->desiredDwell();
        $this->update();
        $this->close($door);
    }

    /**
     * Closes a door.
     * @param Door $door Door object.
     * @return void
     */
    function close(Door $door){
        $this->door = $door;
        $door->close();
        $this->doorState = Dispatcher::DOOR_STATE_CLOSED;
        $this->update();
    }
}
