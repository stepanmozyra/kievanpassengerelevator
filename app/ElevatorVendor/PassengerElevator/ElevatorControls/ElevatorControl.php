<?php
namespace ElevatorVendor\PassengerElevator\ElevatorControls;

/**
 * Interface ElevatorControl
 * @author Artyom Gordiyevsky <kievan@gmail.com>
 */
interface ElevatorControl
{
    /**
     * Updates Dispatcher about various elevator component state changes.
     * @return mixed
     */
    function update();
}