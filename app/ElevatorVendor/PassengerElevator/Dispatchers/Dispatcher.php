<?php
namespace ElevatorVendor\PassengerElevator\Dispatchers;

use ElevatorVendor\PassengerElevator\Drives\Drive as Drive;
use ElevatorVendor\PassengerElevator\Drives\DriveControl as DriveControl;
use ElevatorVendor\PassengerElevator\Doors\DoorControl as DoorControl;

/**
 * Dispatcher is communication hub that processes passenger inputs.
 *
 * Dispatcher does not control actual elevator components, but it is important in the
 * software system. There is one Dispatcher for each car, whose main function is to
 * calculate the target moving direction and destination for the car, as well as to maintain the
 * opening time for the doors.
 *
 * @author Artyom Gordiyevsky <kievan@gmail.com>
 */
class Dispatcher
{
    /**
     * @var Dispatcher The reference to *Dispatcher* instance of this class
     */
    private static $instance;
    /**
     * @var int The idle state of a drive.
     * @const integer The idle state of a drive(DRIVE_STATE_IDLE = 0).
     */
    const DRIVE_STATE_IDLE = 0;
    /**
     * @var int The busy state of a drive.
     * @const int The busy state of a drive(DRIVE_STATE_BUSY = 1).
     */
    const DRIVE_STATE_BUSY = 1;
    /**
     * @var int The up drive destination direction.
     * @const int The up drive destination direction(DRIVE_DEST_DIRECTION_UP = 1).
     */
    const DRIVE_DEST_DIRECTION_UP = 1;
    /**
     * @var int The none drive destination direction.
     * @const int The none drive destination direction(DRIVE_DEST_DIRECTION_NONE = 0).
     */
    const DRIVE_DEST_DIRECTION_NONE = 0;
    /**
     * @var int The down drive destination direction.
     * @const int The down drive destination direction(DRIVE_DEST_DIRECTION_DOWN = -1).
     */
    const DRIVE_DEST_DIRECTION_DOWN = -1;
    /**
     * @var int The drive speed (seconds per floor).
     * @const int The drive speed (seconds per floor, DRIVE_SPEED = 1).
     */
    const DRIVE_SPEED = 1;

    /**
     * @var int The closed door state.
     * @const int The closed door state(DOOR_STATE_CLOSED = 0).
     */
    const DOOR_STATE_CLOSED = 0;
    /**
     * @var int The opened door state.
     * @const int The opened door state(DOOR_STATE_OPENED = 1).
     */
    const DOOR_STATE_OPENED = 1;
    /**
     * @var int The dwell door time of a door (seconds).
     * @const int The dwell door time of a door (seconds, DOOR_DWELL_TIME = 5).
     */
    const DOOR_DWELL_TIME = 5;

    /**
     * @var int The current floor of a drive.
     */
    private static $currentDriveFloor = 0;
    /**
     * @var int The destination drive floor.
     */
    private static $destinationDriveFloor = 0;
    /**
     * @var int The destination drive direction.
     */
    private static $destinationDriveDirection = 0;
    /**
     * @var int The drive state.
     */
    private static $driveState = 0;
    /**
     * @var int The door state.
     */
    private static $doorState = 0;


    /**
     * Returns the *Dispatcher* instance of this class.
     *
     * @return Dispatcher The *Dispatcher* instance.
     */
    public static function getInstance()
    {
        if (null === static::$instance) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    /**
     * Private constructor to prevent creating a new instance of the
     * *Dispatcher* via the `new` operator from outside of this class.
     */
    private function __construct()
    {
    }

    /**
     * Private clone method to prevent cloning of the instance of the
     * *Dispatcher* instance.
     *
     * @return void
     */
    private function __clone()
    {
    }

    /**
     * Private unserialize method to prevent unserializing of the *Dispatcher*
     * instance.
     *
     * @return void
     */
    private function __wakeup()
    {
    }

    /**
     * Returns current drive floor number.
     *
     * @return int The current drive floor number.
     */
    static function getCurrentDriveFloor(){
        return static::$currentDriveFloor;
    }
    /**
     * Returns drive destination floor number.
     *
     * @return int The drive destination floor number.
     */
    static function getDestinationDriveFloor(){
        return static::$destinationDriveFloor;
    }
    /**
     * Returns drive destination direction.
     *
     * @return int The drive destination direction.
     */
    static function getDestinationDriveDirection(){
        return static::$destinationDriveDirection;
    }
    /**
     * Returns drive state.
     *
     * @return int The drive state.
     */
    static function getDriveState(){
        return static::$driveState;
    }
    /**
     * Returns door state.
     *
     * @return int The door state.
     */
    static function getDoorState(){
        return static::$doorState;
    }

    /**
     * Sets current drive floor.
     *
     * @param int $floor Floor number.
     * @return void
     */
    static function setCurrentDriveFloor($floor){
        static::$currentDriveFloor = $floor;
    }
    /**
     * Sets destination drive floor.
     *
     * @param int $floor Floor number.
     * @return void
     */
    static function setDestinationDriveFloor($floor){
        static::$destinationDriveFloor = $floor;
    }
    /**
     * Sets destination drive direction.
     *
     * @param int $direction Drive direction.
     * @return void
     */
    static function setDestinationDriveDirection($direction){
        static::$destinationDriveDirection = $direction;
    }
    /**
     * Sets drive state.
     *
     * @param int $driveState Drive state.
     *
     * @return void
     */
    static function setDriveState($driveState){
        static::$driveState = $driveState;
    }
    /**
     * Sets door state.
     *
     * @param int $doorState Door state.
     * @return void
     */
    static function setDoorState($doorState){
        static::$doorState = $doorState;
    }
    /**
     * Moves drive to the desired floor.
     *
     * Checks the drive state before moving the drive to ensure passenger safety.
     * 
     * @param int $floor Floor number.
     * @return void
     */
    static function desiredFloor($floor) {
        $drive = new Drive(new DriveControl(), new DoorControl());
        echo __CLASS__ . ': get to floor #' . $floor . PHP_EOL;

        if(static::DRIVE_STATE_IDLE === static::getInstance()->getDriveState()) {
            echo 'Moving car to floor #' . $floor . ':' . PHP_EOL;
            $drive->getDriveControl()->move($drive, $floor);
        } else if (static::DRIVE_STATE_BUSY === static::getInstance()->getDriveState()) {
            echo 'Car is busy. Wait.' . PHP_EOL;
        }
    }
    /**
     * Keeps the car door open for a certain period of time, so that passengers can enter.
     *
     * @return void
     */
    static function desiredDwell() {
        echo 'Dwelling: '.PHP_EOL;
        $sleep = static::DOOR_DWELL_TIME/static::DOOR_DWELL_TIME;
        for($i=0; $i < static::DOOR_DWELL_TIME; $i++) {
            if($i != static::DOOR_DWELL_TIME - 1){
                echo '.';
            } else {
                echo '.'.PHP_EOL;
            }
            sleep($sleep);
        }
    }

}
