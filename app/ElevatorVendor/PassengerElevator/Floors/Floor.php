<?php
namespace ElevatorVendor\PassengerElevator\Floors;

/**
 * Class Floor
 * @author Artyom Gordiyevsky <kievan@gmail.com>
 */
class Floor
{
    /**
     * @var FloorButton $floorButton FloorButton object.
     */
    private $floorButton;
    /**
     * @var int $floorNumber Floor number.
     */
    private $floorNumber;

    /**
     * Floor constructor.
     * @param FloorButton $floorButton FloorButton object.
     */
    public function __construct(FloorButton $floorButton){
        $this->floorButton = $floorButton;
        $this->floorNumber = $floorButton->getFloorNumber();
    }

    /**
     * Returns FloorButton object.
     * @return FloorButton
     */
    function getButton(){
        return $this->floorButton;
    }

    /**
     * Return floor number.
     * @return int Floor number.
     */
    function getFloorNumber(){
        return $this->floorNumber;
    }

}