<?php
namespace ElevatorVendor\PassengerElevator\Cars;

use ElevatorVendor\PassengerElevator\ElevatorControls\ElevatorControl as ElevatorControl;
use ElevatorVendor\PassengerElevator\Dispatchers\Dispatcher as Dispatcher;

/**
 * Class CarButtonControl
 * @author Artyom Gordiyevsky <kievan@gmail.com>
 */
class CarButtonControl implements ElevatorControl {
    /**
     * @var int $floorNumber Floor number.
     */
    private $floorNumber;

    /**
     * CarButtonControl constructor.
     */
    function __construct()
    {
    }

    /**
     * Updates Dispatcher with input from passenger.
     *
     * @return void
     */
    function update(){
        echo __CLASS__ . ": sending dispatcher floor number: " . $this->floorNumber . PHP_EOL;
        Dispatcher::getInstance()->setDestinationDriveFloor($this->floorNumber);
    }
    /**
     * Sends the call from inside the car to Dispatcher.
     *
     * @param int $floor Floor number.
     * @return void
     */
    function call($floor){
        $this->floorNumber = $floor;
        $this->update();
        Dispatcher::desiredFloor($this->floorNumber);
    }
}