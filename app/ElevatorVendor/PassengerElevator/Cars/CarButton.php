<?php
namespace ElevatorVendor\PassengerElevator\Cars;

/**
 * Class CarButton
 * @author Artyom Gordiyevsky <kievan@gmail.com>
 */
class CarButton {

    /**
     * @var int $buttonFloorNumber Car button floor number.
     */
    private $buttonFloorNumber;
    /**
     * @var CarButtonControl $carButtonControl Car button control object.
     */
    private $carButtonControl;

    /**
     * CarButton constructor.
     *
     * @param int $floor Floor number.
     * @param CarButtonControl $carButtonControl Car button control object.
     */
    public function __construct($floor, $carButtonControl){
        $this->buttonFloorNumber = $floor;
        $this->carButtonControl = $carButtonControl;
    }

    /**
     * Makes a call from inside the car.
     *
     * @return void
     */
    function carCall(){
        echo 'CAR call to floor #' . $this->buttonFloorNumber . PHP_EOL;
        $this->carButtonControl->call($this->buttonFloorNumber);
    }
}