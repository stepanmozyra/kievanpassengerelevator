<?php
namespace ElevatorVendor\PassengerElevator\Cars;

/**
 * Class Car
 * @author Artyom Gordiyevsky <kievan@gmail.com>
 */
class Car
{
    /**
     * @var array Array of CarButton objects.
     */
    private $carButtons = [];

    /**
     * Car constructor.
     * @param array $carButtons
     */
    function __construct(array $carButtons){
        $this->carButtons = $carButtons;
    }

    /**
     * Gets CarButton object.
     *
     * @param int $floor Floor number.
     * @return CarButton
     */
    function getButton($floor){
        return $this->carButtons[$floor];
    }
}




