<?php
namespace ElevatorVendor\PassengerElevator\Doors;

/**
 * Class DoorMotor
 * @author Artyom Gordiyevsky <kievan@gmail.com>
 */
class DoorMotor {
    /**
     * DoorMotor constructor.
     */
    public function __construct(){
    }

    /**
     * Spins motor to open a door.
     * @return void
     */
    function spinOpen(){
        echo "Spinning door motor to open door.".PHP_EOL;
    }

    /**
     * Spins motor to close a door.
     * @return void
     */
    function spinClose(){
        echo "Spinning door motor to close door.".PHP_EOL;
    }
}